package br.com.roger.utils;

import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableRowSorter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author roger
 */
public class Singleton {

    private String dadosEmpresa;

    private Singleton() {
    }

    private static class SingletonHolder {

        public static final Singleton instance = new Singleton();
    }

    public static Singleton getInstance() {
        return SingletonHolder.instance;
    }

    public void passaCamposComEnter(Component campo) {
        // Colocando enter para pular de campo  
        HashSet conj = new HashSet(campo.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        conj.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
        campo.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, conj);

        //pra invocar  
        //new passaCamposComEnter(panelCampos);  
    }

    public String[] Computador() {
        String info[] = new String[2];
        try {
            InetAddress inet = InetAddress.getLocalHost();
            info[0] = inet.getHostAddress();
            info[1] = inet.getHostName();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
        }
        return info;
    }

    public void mascaraFone(String num, JFormattedTextField campo) {
        String numeros = getNumeros(num);
        if (numeros.length() > 2) {
            int n = Integer.parseInt(numeros.substring(2, 3));
            if (n > 5) {
                try {
                    System.out.println("9 digitos...." + n);
                    MaskFormatter mascaraFone = new MaskFormatter("(##)#####-####");
                    campo.setFormatterFactory(new DefaultFormatterFactory(mascaraFone));
                    campo.setText(numeros);
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao validar telefone");
                }
            } else if (n <= 5) {
                try {
                    System.out.println("8 digitos..." + n);
                    MaskFormatter mascaraFone = new MaskFormatter("(##)####-####");
                    campo.setFormatterFactory(new DefaultFormatterFactory(mascaraFone));
                    campo.setText(numeros);
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao validar telefone");
                }
            }
        }
    }

    public static boolean soContemNumeros(String texto) {
        return texto.matches("[0-9]");
    }

    public String limpaMascaraFone(String fone) {
        fone = fone.replace('(', ' ');
        fone = fone.replace(')', ' ');
        fone = fone.replace('-', ' ');
        return fone;
    }

    public void filtroSorter(TableRowSorter sorter, String text, int coluna) {
        if (text.length() == 0) {
            sorter.setRowFilter(null);
        } else {
            sorter.setRowFilter(RowFilter.regexFilter("(?i)" + text, coluna));
        }
    }

    public void copyFile(File source, File destination) throws IOException {
        if (destination.exists()) {
            destination.delete();
        }
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen()) {
                sourceChannel.close();
            }
            if (destinationChannel != null && destinationChannel.isOpen()) {
                destinationChannel.close();
            }
        }
    }

    public void abrirSite(String site) {
        try {
            try {
                java.awt.Desktop.getDesktop().browse(new java.net.URI(site));
            } catch (URISyntaxException ex) {
                Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean temNumeros(String texto) {
        for (int i = 0; i < texto.length(); i++) {
            if (Character.isDigit(texto.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public File buscarDiretorio() {
        File diretorio = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int res = chooser.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            diretorio = chooser.getSelectedFile();
        }
        return diretorio;
    }

    public String buscarArquivoImagem() {
        String arquivo = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & PNG Images", new String[]{"jpg", "png"});
        chooser.setFileFilter(filter);
        int res = chooser.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            arquivo = chooser.getSelectedFile().getAbsolutePath();
        }
        return arquivo;
    }

    public ImageIcon redimensionaImg(File image, int new_w, int new_h) {
        try {
            BufferedImage imagem = ImageIO.read(image);
            BufferedImage new_img = new BufferedImage(new_w, new_h, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = new_img.createGraphics();
            g.drawImage(imagem, 0, 0, new_w, new_h, null);
            g.dispose();
            return new ImageIcon(new_img);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //adicionar, alterar, excluir
    public String iconeJTable(String opcao) {
        String caminho = "";
        if (opcao.equalsIgnoreCase("alterar")) {
            caminho = "iconeAterar16x16.png";
        } else if (opcao.equalsIgnoreCase("excluir")) {
            caminho = "iconeExcluir16x16.png";
        } else if (opcao.equalsIgnoreCase("adicionar")) {
            caminho = "iconeAdicionar16x16.png";
        } else if (opcao.equalsIgnoreCase("gerar")) {
            caminho = "iconeRefresh16x16.png";
        }
        return diretorioImagens() + caminho;
    }

    public String diretorioImagens() {
        String caminho = "";
        caminho = "/br/com/roger/gui/imagem/";
        return caminho;
    }

    public double converteStringParaDouble(String valor) {
        double value = 0;
        if (valor != null && !valor.trim().isEmpty()) {
            value = Double.parseDouble(valor.replace(",", "."));
        }
        return value;
    }

    public String converteDoubleParaString(Double valor, int casasDecimais) {

        String resultado = "";
        if (valor != null) {
            resultado = String.format("%." + casasDecimais + "f", valor);
            resultado = resultado.replace(".", ",");
        }
        return resultado;
    }

    public Date converteDataParaInicio(Date data) {
        return null;

    }

    public Date converteStringParaData(String data) {
        if (data == null || data.equals("")) {
            return null;
        }
        Date date = null;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = (java.util.Date) formatter.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
        }

        return date;
    }

    public String converteDataParaString(Date data) {
        String dt = "";
        if (data != null) {
            SimpleDateFormat formataData = new SimpleDateFormat("dd/MM/yyyy");
            dt = formataData.format(data);
        }
        return dt;
    }

    public String converteDataParaStringNumerico(Date data) {
        String dt = "";
        if (data != null) {
            SimpleDateFormat formataData = new SimpleDateFormat("ddMMyyyy");
            dt = formataData.format(data);
        }
        return dt;
    }

    public String converteDataParaStringNumericoDataHora(Date data) {
        String dt = "";
        if (data != null) {
            SimpleDateFormat formataData = new SimpleDateFormat("ddMMyyyyHHmmss");
            dt = formataData.format(data);
        }
        return dt;
    }

    public Date retornaPrimeiroDiaMes(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public Date retornaUltimoDiaMes(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public Date somaMes(Date d, int soma) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + soma);
        return cal.getTime();
    }

    public Date somaDia(Date d, int soma) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.DATE, soma);
        return cal.getTime();
    }

    public String caminhoSistema() {
        return System.getProperty("user.dir");
    }

    public String caminhoPDF() {
        return System.getProperty("user.dir") + File.separator + "pdf";
    }

    public String caminhoLogo() {
        return System.getProperty("user.dir") + File.separator + "logo.png";
    }

    public String caminhoMysqldump() {
        return System.getProperty("user.dir") + File.separator +  "mysqldump.exe";
    }

    public String caminhoLembrarSenha() {
        return System.getProperty("user.dir") + File.separator + "lembrar.txt";
    }

    public File lembrarSenha() {
        String lembrar = caminhoLembrarSenha();
        File f = new File(lembrar);
        return f;
    }

    public boolean confirma(String msg) {
        int result = JOptionPane.showConfirmDialog(null, msg, "", JOptionPane.YES_NO_OPTION);
        return result == JOptionPane.YES_OPTION;
    }

    public String preencheString(String campo, int tamanho, String caracter) {
        while (campo.length() < tamanho) {
            campo += caracter;
        }
        return campo;
    }

    public String zeroFill(int num, int size) {
        String format = String.format("%0" + size + "d", num);
        return format;
    }

    public String getNumeros(String str) {
        if (str != null) {
            return str.replaceAll("[^0123456789]", "");
        } else {
            return "";
        }
    }

    public int convertToNumber(String numero) {
        int num = Integer.parseInt(numero);
        return num;
    }

    public String getDadosEmpresa() {
        return dadosEmpresa;
    }

    public void setDadosEmpresa(String dadosEmpresa) {
        this.dadosEmpresa = dadosEmpresa;
    }

    public String dadosEmpresa() {
        return dadosEmpresa;
    }

    public String criptografaSenha(char[] senhaCripto) {
        String senha = new String(senhaCripto);
        senha = SHACheckSum.hash256(senha);
        return senha;
    }

    public void gravarArquivo(File local, String texto, boolean msgConcluido) {
        String conteudo = texto;
        try {
            // o true significa q o arquivo será constante
            FileWriter x = new FileWriter(local, true);
            conteudo += "\n\r"; // criando nova linha e recuo no arquivo			
            x.write(conteudo); // armazena o texto no objeto x, que aponta para o arquivo			
            x.close(); // cria o arquivo
            if (msgConcluido) {
                JOptionPane.showMessageDialog(null, "Arquivo gravado com sucesso", "Concluído", JOptionPane.INFORMATION_MESSAGE);
            } // em caso de erro apreenta mensagem abaixo
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Atenção", JOptionPane.WARNING_MESSAGE);
        }
    }

    public String lerArquivo(File f) {
        String linha = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));
            while (reader.ready()) {
                linha += reader.readLine();
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return linha;
    }
    
    public double arredondar(double valor, String formato) {
        DecimalFormat df = new DecimalFormat(formato);
        df.setRoundingMode(RoundingMode.HALF_UP);
        return this.converteStringParaDouble(df.format(valor));
     }
}
