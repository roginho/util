package br.com.roger.utils;
import javax.swing.text.*;  
  
public class LimitaCaracteres extends PlainDocument   
{  
    private int iMaxLength;  
  
    
    public LimitaCaracteres(int maxlen) {  
        super();  
        iMaxLength = maxlen;  
    }  
  
    @Override  
    public void insertString(int offset, String str, AttributeSet attr)  
                    throws BadLocationException {  
        //if (s == null) return;  
  
        if (iMaxLength <= 0)        // aceitara qualquer no. de caracteres  
        {  
            super.insertString(offset, str.toUpperCase(), attr);  
            return;  
        }  
  
        int ilen = (getLength() + str.length());  
        if (ilen <= iMaxLength)    // se o comprimento final for menor...  
            super.insertString(offset, str.toUpperCase(), attr);   // ...aceita str  
        }  
//uso
            //seuTextField.setDocument(new LimitaCaracteres(45));

}  