package br.com.roger.utils;


import java.security.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SHACheckSum {

    public static String hash256(String senha)  {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(senha.getBytes());
            return bytesToHex(md.digest());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SHACheckSum.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte byt : bytes) {
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }
}
