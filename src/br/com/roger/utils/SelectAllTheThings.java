/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.roger.utils;

import java.awt.Component;  
import java.awt.Container;  
import java.awt.event.FocusAdapter;  
import java.awt.event.FocusEvent;  
import java.awt.event.FocusListener;  
import javax.swing.text.JTextComponent;  
  
public class SelectAllTheThings extends FocusAdapter {  
  
    private static FocusListener f;  
  
    /** 
     * Construtor privado para não ser instaciado fora da classe 
     */  
    private SelectAllTheThings() {  
    }  
  
    /** 
     * Evento focusGained, que vai ser executado quando o usuário 
     * selecionar o campo de texto, que vai fazer todo o texto 
     * (se tiver algum) ser selecionado automaticamente 
     */  
    public void focusGained(FocusEvent e) {  
        Object o = e.getSource();  
  
        if (o instanceof JTextComponent) {  
            JTextComponent j = (JTextComponent) o;  
            j.selectAll();  
        }  
    }  
  
    /** 
     * @return A única instância de SelectAllTheThings 
     */  
    public static FocusListener getListener() {  
        if (f == null) {  
            f = new SelectAllTheThings();  
        }  
        return f;  
    }  
  
    /** 
     * Dado um Conteiner, adiciona o FocusListener para todos os 
     * componentes de texto dentro do conteiner, e de quaisquer 
     * Conteirners ascendentes. 
     *  
     * @param o Conteiner ancestral de todos os componentes de texto 
     * que se quer adicionar o FocusListener 
     */  
    public static void addListeners(Container c) {  
  
        if (c instanceof JTextComponent) {  
            JTextComponent j = (JTextComponent) c;  
            j.addFocusListener(getListener());  
        } else {  
            for (Component comp : c.getComponents()) {  
                if (comp instanceof Container) {  
                    addListeners((Container) comp);  
                }  
            }  
        }  
  
    }  
} 


